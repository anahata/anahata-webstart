package com.anahata.webstart.sign;

import com.anahata.webstart.util.IOUtil;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import static com.anahata.webstart.AbstractBaseJnlpMojo.*;

/**
 * Created by twebster on 13/03/2015.
 */
public class ConcurrentJarSigner {

    int threads;
    ExecutorService executor;

    public ConcurrentJarSigner() {
        this(Runtime.getRuntime().availableProcessors());
    }

    public ConcurrentJarSigner(int threads) {
        this.threads = threads;
        this.executor = Executors.newFixedThreadPool(threads);
    }

    public void execute( File[] jarFiles, IOUtil ioUtil, SignTool signTool,
                         SignConfig signConfig, Log logger, boolean signVerify ) throws MojoExecutionException {

        logger.info("Signing jars with " + threads + " threads");

        long startTime = System.nanoTime();
        for ( File jarFile : jarFiles ) {
            Runnable task = new SignJarRunnable(jarFile, ioUtil, signTool, signConfig, logger, signVerify);
            executor.execute(task);
        }

        this.executor.shutdown();

        try {
            this.executor.awaitTermination(60, TimeUnit.MINUTES);
        } catch ( InterruptedException e ) {
            throw new MojoExecutionException("Error shutting down ConcurrentJarSigner", e);
        }
        finally {
            long endTime = System.nanoTime();
            long elapsedTime = (endTime - startTime) / 1000000000;
            logger.info("Signing took " + elapsedTime + " seconds.");
        }
    }

    private class SignJarRunnable implements Runnable {

        private File jarFile;
        private IOUtil ioUtil;
        private SignTool signTool;
        private SignConfig signConfig;
        private Log logger;
        private boolean signVerify;

        private SignJarRunnable( File jarFile, IOUtil ioUtil, SignTool signTool,
                                 SignConfig signConfig, Log logger, boolean signVerify ) {
            this.jarFile = jarFile;
            this.ioUtil = ioUtil;
            this.signTool = signTool;
            this.signConfig = signConfig;
            this.logger = logger;
            this.signVerify = signVerify;
        }

        public void run() {
            File signedJar = toProcessFile( jarFile );
            try {
                ioUtil.deleteFile( signedJar );
                logger.info( "Sign " + signedJar.getName());
                signTool.sign(signConfig, jarFile, signedJar);
                logger.info("lastModified signedJar:" + signedJar.lastModified() + " unprocessed signed Jar:" +
                        jarFile.lastModified());

                if ( signVerify )
                {
                    logger.info( "Verify signature of " + signedJar.getName() );
                    signTool.verify( signConfig, signedJar, true );
                }
                // remove unprocessed files
                // TODO wouldn't have to do that if we copied the unprocessed jar files in a temporary area
                ioUtil.deleteFile(jarFile);

            } catch (MojoExecutionException e) {
               logger.error("Jar signing failed! " + e.getMessage());
            }
        }

        private File toProcessFile( File source )
        {
            if ( !source.getName().startsWith( UNPROCESSED_PREFIX ) )
            {
                throw new IllegalStateException( source.getName() + " does not start with " + UNPROCESSED_PREFIX );
            }
            String targetFilename = source.getName().substring(UNPROCESSED_PREFIX.length());
            return new File( source.getParentFile(), targetFilename );
        }
    }
}
