/*
 * Copyright 2015 Codehaus.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.webstart.dependency.filenaming;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.maven.artifact.Artifact;

/**
 *
 * @author pablo
 */
public class UniqueSnapshotVersionUtils
{

    public static final String getUniqueVersion( Artifact artifact )
    {
        if (!artifact.getVersion().equals( artifact.getBaseVersion())) {
            return artifact.getVersion();
        } else if (artifact.getBaseVersion().toUpperCase().contains("SNAPSHOT")) {
            Date d = new Date(artifact.getFile().lastModified());
            DateFormat df = new SimpleDateFormat("yyyyMMdd.HHmmss");
            return artifact.getBaseVersion() + "." + df.format(d) + "-local-maven-repo";
        } else {
            return artifact.getVersion();
        }

    }
}
