package com.anahata.webstart.util;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import java.io.*;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.FileSystem;
import java.util.*;
import java.util.Map.Entry;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;

/**
 * Created on 10/26/13.
 *
 * @author Tony Chemit <chemit@codelutin.com>
 * @since 1.0-beta-4
 */
@Component(role = JarUtil.class, hint = "default")
public class DefaultJarUtil
        implements JarUtil {

    /**
     * io helper.
     */
    @Requirement
    protected IOUtil ioUtil;

    /**
     * {@inheritDoc}
     */
    public void updateManifestEntries(File jar, Map<String, String> manifestentries)
            throws MojoExecutionException {

        Manifest manifest = createManifest(jar, manifestentries);

        File updatedUnprocessedJarFile = new File(jar.getParent(), jar.getName() + "_updateManifestEntriesJar");

        ZipFile originalJar = null;
        JarOutputStream targetJar = null;

        try {
            originalJar = new ZipFile(jar);
            targetJar = new JarOutputStream(new FileOutputStream(updatedUnprocessedJarFile), manifest);

            // add all other entries from the original jar file
            Enumeration<? extends ZipEntry> entries = originalJar.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();

                // skip the original manifest
                if (JarFile.MANIFEST_NAME.equals(entry.getName())) {
                    continue;
                }

                ZipEntry newEntry = new ZipEntry(entry.getName());
                targetJar.putNextEntry(newEntry);

                // write content to stream if it is a file
                if (!entry.isDirectory()) {
                    InputStream inputStream = null;
                    try {
                        inputStream = originalJar.getInputStream(entry);
                        org.codehaus.plexus.util.IOUtil.copy(inputStream, targetJar);
                        inputStream.close();
                    } finally {
                        org.apache.maven.shared.utils.io.IOUtil.close(inputStream);
                    }
                }
                targetJar.closeEntry();
            }
            targetJar.close();
            originalJar.close();
        } catch (IOException e) {
            throw new MojoExecutionException("Error while updating manifest of " + jar.getName(), e);
        } finally {
            org.apache.maven.shared.utils.io.IOUtil.close(targetJar);
            ioUtil.close(originalJar);
        }

        // delete incoming jar file
        ioUtil.deleteFile(jar);

        // rename patched jar to incoming jar file
        ioUtil.renameTo(updatedUnprocessedJarFile, jar);
    }

    /**
     * Create the new manifest from the existing jar file and the new entries
     *
     * @param jar
     * @param manifestentries
     * @return Manifest
     * @throws MojoExecutionException
     */
    protected Manifest createManifest(File jar, Map<String, String> manifestentries)
            throws MojoExecutionException {
        JarFile jarFile = null;
        try {
            jarFile = new JarFile(jar);

            // read manifest from jar
            Manifest manifest = jarFile.getManifest();

            if (manifest == null || manifest.getMainAttributes().isEmpty()) {
                manifest = new Manifest();
                manifest.getMainAttributes().putValue(Name.MANIFEST_VERSION.toString(), "1.0");
            }

            //System.out.println("Main attributes");
//            List<Object> toBeRemoved = new ArrayList();
//            for (Object o : manifest.getMainAttributes().keySet()) {
//                //System.out.println(o + " = " + manifest.getMainAttributes().get(o));
//                if (o.toString().contains("Bundle") || o.toString().contains("Bnd")) {
//                    //System.out.println("Will remove " + o);
//                    toBeRemoved.add(o);
//                }
//            }
//            //System.out.println("-----Entries-------");
//            for (String o : manifest.getEntries().keySet()) {
//                //System.out.println(o + " = " + manifest.getEntries().get(o).values());
//            }
            if (manifest.getMainAttributes().containsKey(Name.CLASS_PATH)) {
                System.out.println("Removing " + Name.CLASS_PATH + " MANIFEST entry from " + jar);
                manifest.getMainAttributes().remove(Name.CLASS_PATH);
            }

//            manifest.getMainAttributes().remove(new Name("Export-Package"));            
//            manifest.getMainAttributes().remove(new Name("Import-Package"));            
//            for (Object toBeRemoved1 : toBeRemoved) {
//                manifest.getMainAttributes().remove(toBeRemoved1);
//            }
//            manifest.getMainAttributes().remove(new Name("Bundle-Name"));
//            manifest.getMainAttributes().remove(new Name("Bundle-Description"));
//            manifest.getMainAttributes().remove(new Name("Bundle-Description"));
            // add or overwrite entries
            Set<Entry<String, String>> entrySet = manifestentries.entrySet();
            for ( Entry<String, String> entry : entrySet )
            {
                manifest.getMainAttributes().putValue( entry.getKey(), entry.getValue() );
            }
            
            System.out.println("-----Removing entries with signatures-------");
            for (String key : new ArrayList<>(manifest.getEntries().keySet())) {        
                Attributes atts = manifest.getEntries().get(key);
                for (Entry<Object, Object> entry : atts.entrySet()) {
                    if (entry.getKey().toString().toLowerCase().contains("digest")) {
                        Attributes ret = manifest.getEntries().remove(key);
                        System.out.println("Removed " + ret.keySet() + " " + ret.values());
                        break;
                    }
                }
            }

            System.out.println("Final Manifest for " + jar);

            System.out.println("------ Main attributes-----");
            for (Object o : manifest.getMainAttributes().keySet()) {
                System.out.println(o + " = " + manifest.getMainAttributes().get(o));
            }
            
            System.out.println("-----Entries-------");
            for (String o : manifest.getEntries().keySet()) {
                System.out.println(o + " = " + manifest.getEntries().get(o).values());
            }

            return manifest;
        } catch (IOException e) {
            throw new MojoExecutionException("Error while reading manifest from " + jar.getAbsolutePath(), e);
        } finally {
            ioUtil.close(jarFile);
        }
    }

    public void addFile(File jar, File fileToAdd, String zipEntryName)
            throws MojoExecutionException {
        File updatedUnprocessedJarFile = new File(jar.getParent(), jar.getName() + "_addFileJar");

        ZipFile originalJar = null;
        JarOutputStream targetJar = null;

        try {
            originalJar = new ZipFile(jar);
            targetJar = new JarOutputStream(new FileOutputStream(updatedUnprocessedJarFile));

            // add all other entries from the original jar file
            Enumeration<? extends ZipEntry> entries = originalJar.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();

                // if the entry to write is already in the zip, skip it
                if (zipEntryName.equals(entry.getName())) {
                    continue;
                }

                ZipEntry newEntry = new ZipEntry(entry.getName());
                targetJar.putNextEntry(newEntry);

                // write content to stream if it is a file
                if (!entry.isDirectory()) {
                    InputStream inputStream = null;
                    try {
                        inputStream = originalJar.getInputStream(entry);
                        org.codehaus.plexus.util.IOUtil.copy(inputStream, targetJar);
                        inputStream.close();
                    } finally {
                        org.apache.maven.shared.utils.io.IOUtil.close(inputStream);
                    }
                }
                targetJar.closeEntry();
            }

            // Now add the new entry
            ZipEntry newEntry = new ZipEntry(zipEntryName);
            targetJar.putNextEntry(newEntry);
            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(fileToAdd);
                org.codehaus.plexus.util.IOUtil.copy(inputStream, targetJar);
                inputStream.close();
            } finally {
                org.apache.maven.shared.utils.io.IOUtil.close(inputStream);
            }

            targetJar.close();
            originalJar.close();
        } catch (IOException e) {
            throw new MojoExecutionException(
                    "Error while adding " + fileToAdd.getAbsolutePath() + " as " + zipEntryName + " to " + jar.getName(),
                    e);
        } finally {
            org.apache.maven.shared.utils.io.IOUtil.close(targetJar);
            ioUtil.close(originalJar);
        }

        // delete incoming jar file
        ioUtil.deleteFile(jar);

        // rename patched jar to incoming jar file
        ioUtil.renameTo(updatedUnprocessedJarFile, jar);
    }

    public void deleteFile(File jar, String location) throws MojoExecutionException {
        /* Define ZIP File System Properies in HashMap */
        Map<String, String> zip_properties = new HashMap<>();
        /* We want to read an existing ZIP File, so we set this to False */
        zip_properties.put("create", "false");

        /* Specify the path to the ZIP File that you want to read as a File System */
        String absolutePath = jar.getAbsolutePath();
        if (!absolutePath.startsWith("/")) {
            absolutePath = "///" + absolutePath.replace("\\", "/");
        }
        URI zip_disk = URI.create("jar:file:" + absolutePath);

        System.out.println("deleting " + zip_disk);

        /* Create ZIP file System */
        try (FileSystem zipfs = FileSystems.newFileSystem(zip_disk, zip_properties)) {
            /* Get the Path inside ZIP File to delete the ZIP Entry */
            Path pathInZipfile = zipfs.getPath(location);
            /* Execute Delete */
            if (Files.exists(pathInZipfile)) {
                System.out.println("About to delete  " + location);
                Files.delete(pathInZipfile);
            }

            System.out.println("File successfully deleted");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            throw new MojoExecutionException("Exception deleting INDEX.LIST", ioe);
        }
    }

    public static void main(String[] args) throws Exception {

        URI zip_disk = new URI(
                "jar:file:///E:/Projects/links/links-bms/links-bms-web/target/jnlp/lib/unprocessed_ch.qos.cal10n-cal10n-api__V0.8.1.jar");

        System.out.println("deleting " + zip_disk);

        Map<String, String> zip_properties = new HashMap<>();
        /* We want to read an existing ZIP File, so we set this to False */
        zip_properties.put("create", "false");

        /* Create ZIP file System */
        //URI zipfile = URI.create("jar:file:/tmp/anahata-jws-cache/ch.qos.cal10n-cal10n-api__V0.8.1.jar");
        try (FileSystem zipfs = FileSystems.newFileSystem(zip_disk, zip_properties, null)) {

        }
        //new DefaultJarUtil().deleteFile(new File("/tmp/anahata-jws-cache/ch.qos.cal10n-cal10n-api__V0.8.1.jar"), null);
    }
}
