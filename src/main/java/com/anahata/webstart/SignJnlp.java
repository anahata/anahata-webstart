package com.anahata.webstart;

import org.apache.maven.plugin.MojoExecutionException;

public class SignJnlp {

    private JnlpArtifact jnlpArtifact;

    public JnlpArtifact getJnlpArtifact() {
        if ( jnlpArtifact == null ) {
            return JnlpArtifact.FILE;
        }
        return jnlpArtifact;
    }

    public void setJnlpArtifact( String jnlpArtifact ) throws MojoExecutionException {
        this.jnlpArtifact = JnlpArtifact.fromString(jnlpArtifact);
    }
}
