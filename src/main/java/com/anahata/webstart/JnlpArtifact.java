package com.anahata.webstart;

import org.apache.maven.plugin.MojoExecutionException;

public enum JnlpArtifact {

    /**
     * An APPLICATION.JNLP artifact
     */
    FILE,

    /**
     * An APPLICATION_TEMPLATE.JNLP artifact
     */
    TEMPLATE;

    public static JnlpArtifact fromString( String artifact ) throws MojoExecutionException {
        if ( artifact == null || "".equals(artifact)) {
            return FILE;
        }
        else if ( artifact.equalsIgnoreCase("file") ) {
            return FILE;
        }
        else if ( artifact.equalsIgnoreCase("template") ) {
            return TEMPLATE;
        }
        else {
            String msg = "Invalid JNLP artifact type: " + artifact + ". Valid options are 'file' and 'template'";
            throw new MojoExecutionException( msg );
        }
    }
}
