package com.anahata.webstart;

import com.anahata.webstart.JnlpArtifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import static org.junit.Assert.*;

public class JnlpArtifactTest {

    @Test
    public void testFromStringNull() throws Exception {
        JnlpArtifact jnlpArtifact = JnlpArtifact.fromString(null);
        assertEquals(JnlpArtifact.FILE, jnlpArtifact);
    }

    @Test
    public void testFromEmptyString() throws Exception {
        JnlpArtifact jnlpArtifact = JnlpArtifact.fromString("");
        assertEquals(JnlpArtifact.FILE, jnlpArtifact);
    }

    @Test
    public void testFromStringfile() throws Exception {
        JnlpArtifact jnlpArtifact = JnlpArtifact.fromString("file");
        assertEquals(JnlpArtifact.FILE, jnlpArtifact);
    }

    @Test
    public void testFromStringFILE() throws Exception {
        JnlpArtifact jnlpArtifact = JnlpArtifact.fromString("FILE");
        assertEquals(JnlpArtifact.FILE, jnlpArtifact);
    }

    @Test
    public void testFromStringtemplate() throws Exception {
        JnlpArtifact jnlpArtifact = JnlpArtifact.fromString("template");
        assertEquals(JnlpArtifact.TEMPLATE, jnlpArtifact);
    }

    @Test
    public void testFromStringTEMPLATE() throws Exception {
        JnlpArtifact jnlpArtifact = JnlpArtifact.fromString("TEMPLATE");
        assertEquals(JnlpArtifact.TEMPLATE, jnlpArtifact);
    }

    @Test(expected = MojoExecutionException.class)
    public void testFromStringInvalid() throws Exception {
        JnlpArtifact.fromString("invalid value");
    }
}