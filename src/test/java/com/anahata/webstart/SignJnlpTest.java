package com.anahata.webstart;

import com.anahata.webstart.SignJnlp;
import com.anahata.webstart.JnlpArtifact;
import org.junit.Test;

import static org.junit.Assert.*;

public class SignJnlpTest {

    @Test
    public void testGetSetJnlpArtifact() throws Exception {
        SignJnlp testSubject = new SignJnlp();
        testSubject.setJnlpArtifact("file");

        assertEquals(JnlpArtifact.FILE, testSubject.getJnlpArtifact());
    }

    @Test
    public void testGetSetJnlpArtifactWhenNull() throws Exception {
        SignJnlp testSubject = new SignJnlp();
        assertEquals(JnlpArtifact.FILE, testSubject.getJnlpArtifact());
    }

}